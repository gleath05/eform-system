import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaveFormsComponent } from './components/leave-forms/leave-forms.component';
import { ShiftSwapFormComponent } from './components/shift-swap-form/shift-swap-form.component';


import { OverTimeFormComponent } from './components/over-time-form/over-time-form.component';
import { BadgeCorrectionComponent } from './components/badge-correction/badge-correction.component';
import { MyScheduleComponent } from './components/my-schedule/my-schedule.component';
import { NewListOfRequestsFormComponent } from './components/new-list-of-requests-form/new-list-of-requests-form.component';
import { TeamScheduleComponent } from './components/team-schedule/team-schedule.component';
import { EFormsApprovalComponent } from './components/e-forms-approval/e-forms-approval.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';




const routes: Routes = [

  {
    path: 'login', component: SideNavComponent, children: [
      { path: 'leave-form', component: LeaveFormsComponent },
      { path: 'my-schedule', component: MyScheduleComponent },
      { path: 'overtime-form', component: OverTimeFormComponent },
      { path: 'shift-swap-form', component: ShiftSwapFormComponent },
      { path: 'badge-correction', component: BadgeCorrectionComponent },
      { path: 'list-of-requests-form', component: NewListOfRequestsFormComponent },
      { path: 'team-schedule', component: TeamScheduleComponent },
      { path: 'eforms-approval', component: EFormsApprovalComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjAppRoutingModule { }
