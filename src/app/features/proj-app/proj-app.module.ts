import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// module
import { ProjAppRoutingModule } from './proj-app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// components
import { LeaveFormsComponent } from './components/leave-forms/leave-forms.component';
import { MainMenuComponent } from './container/main-menu/main-menu.component';
import { ShiftSwapFormComponent } from './components/shift-swap-form/shift-swap-form.component';
import { BadgeCorrectionComponent } from './components/badge-correction/badge-correction.component';
import { OverTimeFormComponent } from './components/over-time-form/over-time-form.component';

//Material Modules
import { MatRadioModule, MatFormFieldModule, MatInputModule, MatCardModule, MatButtonModule, MatSidenavModule, MatToolbarModule, MatIconModule, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule, MatTabsModule, MatOptionModule, MatSelectModule, MatTableModule, MatListModule, MatDialog, MatDialogModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MyScheduleComponent } from './components/my-schedule/my-schedule.component';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { NewListOfRequestsFormComponent } from './components/new-list-of-requests-form/new-list-of-requests-form.component';

import { TeamScheduleComponent } from './components/team-schedule/team-schedule.component';
import { EFormsApprovalComponent } from './components/e-forms-approval/e-forms-approval.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { DialogLogoutComponent } from './components/dialog-logout/dialog-logout.component';
import { LoginComponent } from './components/login/login.component';



@NgModule({
  declarations: [
    OverTimeFormComponent,
    LeaveFormsComponent,
    BadgeCorrectionComponent,
    MyScheduleComponent,
    ShiftSwapFormComponent,
    NewListOfRequestsFormComponent,
    TeamScheduleComponent,
    EFormsApprovalComponent,
    SideNavComponent,
    DialogLogoutComponent,


  ],
  entryComponents: [DialogLogoutComponent],
  imports: [
    CommonModule,
    NgbModalModule,
    FormsModule,
    FlatpickrModule.forRoot(),
    ReactiveFormsModule,
    ProjAppRoutingModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    FlexLayoutModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatTabsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatButtonModule,
    MatListModule,
    MatDialogModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [SideNavComponent],
})
export class ProjAppModule { }
