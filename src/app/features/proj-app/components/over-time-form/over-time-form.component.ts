import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

export interface Shift {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-over-time-form',
  templateUrl: './over-time-form.component.html',
  styleUrls: ['./over-time-form.component.scss']
})
export class OverTimeFormComponent implements OnInit {

  shiftChosen: string;
  otForm: FormGroup;
  test: FormGroup
  isSubmitted: boolean;
  submitted = false;
  minDate = new Date();

  startTime: any;
  endTime: any;
  
  totalOtHours: any;

  startDate = new Date().toISOString().substring(0,10);
  endDate = new Date().toISOString().substring(0,10);

  endTimeDate:any;
  startTimeDate:any;


  constructor(private fb: FormBuilder) {
  }

  //select dropdown value
  shifts: Shift[] = [
    { value: 'preShiftOT', viewValue: 'Pre-Shift Overtime' },
    { value: 'postShiftOT', viewValue: 'Post-Shift Overtime' }
  ];

  ngOnInit() {
    this.submitOT();

  }

  submitOT() {
    this.otForm = this.fb.group({
      shiftDate: [null],
      startdate: [null, Validators.required],
      enddate: [null, Validators.required],
      shift: [null, Validators.required],
      startTime: [null, Validators.required],
      endTime: [null, Validators.required],
      result: [''],
      reason: [null, Validators.required],
    });
    this.resetForm();
  }

  get formControls() {
    return this.otForm.controls;
  }

  resetForm() {
    this.otForm.reset();
    this.otForm.patchValue({
      startdate: '',
      enddate: '',
      shift: '',
      startTime: '',
      endTime: '',
      reason: '',
      result: ''
    });
    this.otForm.markAsUntouched();
  }

  onSubmit() {
    this.isSubmitted = true;
     console.log(this.otForm.value);

  }
  getstartTime(time: any) {
    this.startTime =  time;

    this.startTimeDate = new Date(this.startDate+'T'+this.startTime);
    this.endTimeDate = new Date(this.endDate+'T'+this.endTime);

    this.totalOtHours = Math.abs((this.startTimeDate.getTime() - this.endTimeDate.getTime()) / 60000) +' '+ 'Minutes';
  }

  getendTime(time: any) {
    this.endTime = time;

    this.startTimeDate = new Date(this.startDate+'T'+this.startTime);
    this.endTimeDate = new Date(this.endDate+'T'+this.endTime);

    this.totalOtHours = Math.abs((this.startTimeDate.getTime() - this.endTimeDate.getTime()) / 60000) +' '+ 'Minutes';
  }


}
