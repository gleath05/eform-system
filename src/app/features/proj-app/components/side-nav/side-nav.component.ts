import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialogModule, MatDialog } from '@angular/material';
import { DialogLogoutComponent } from '../dialog-logout/dialog-logout.component';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  empName = 'James Lee Tabao';
  screenWidth: number;
  static adminUser: boolean;
  disabledBtn: boolean = SideNavComponent.adminUser;
  
  
  constructor(public dialog: MatDialog) {
    //for handling 
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
    }
  }

  ngOnInit() {
  }

  openConfirmDialog() { 
    this.dialog.open(DialogLogoutComponent, {
    });
  }

  openLogoutDialog() { 
    this.openConfirmDialog();
  }
} 