import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SideNavComponent } from '../side-nav/side-nav.component';
import { user } from 'src/app/features/shared/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
/* static try:boolean; */
  
  

  constructor(private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private user: SideNavComponent) { }

  ngOnInit() {
    this.login();
  }

  private login() {
    this.submitLogin();
  }

  submitLogin() {
    this.loginForm = this.fb.group({
      userName: ['', [Validators.required]],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    if (this.loginForm.controls.userName.value === 'admin' && this.loginForm.controls.password.value === 'admin') {
      this.user.disabledBtn = true;
      SideNavComponent.adminUser = this.user.disabledBtn;
      this.toastr.success('Login Success!');
      this.router.navigate(['login/my-schedule']);

    } else if (this.loginForm.controls.userName.value === 'emp1' && this.loginForm.controls.password.value === 'emp1') {
      this.user.disabledBtn = false;
      SideNavComponent.adminUser = this.user.disabledBtn;
      this.toastr.success('Login Success!');
      this.router.navigate(['login/my-schedule']);

    }
    else {
      this.toastr.error('please input correct credentials')
    }
  }

} 