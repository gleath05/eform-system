import { Component, OnInit } from '@angular/core';
import { Toast, ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialog-logout',
  templateUrl: './dialog-logout.component.html',
  styleUrls: ['./dialog-logout.component.scss']
})
export class DialogLogoutComponent implements OnInit {

  constructor(private toastr: ToastrService,
  private router: Router) { }

  ngOnInit() {
  }

  logOut() { 
    this.router.navigate(['login']);
    this.toastr.warning('Logout Success')
  }

}
