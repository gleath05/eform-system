
import {
  Component, OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  startOfWeek,
  endOfWeek,
  addWeeks,
  isMonday,
  endOfHour,
  startOfHour,
  setHours,
  setMinutes,
  startOfMonth,
  startOfSecond,
  endOfToday,
  addMinutes,
  format,
  subMinutes,
  subHours,
  isAfter,
  isFuture,
  startOfToday
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  MOMENT,


} from 'angular-calendar';
import { WeekDay } from '@angular/common';





const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
  ,
  black: {
    primary: '#10e301',
    secondary: '#FDF1BC'
  }
};

@Component({
  selector: 'app-my-schedule',
  templateUrl: './my-schedule.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./my-schedule.component.scss']
})
export class MyScheduleComponent implements OnInit {



  view: CalendarView = CalendarView.Month;

  weekStartsOn: number = 1;
  excludeDats: number[] = [];
  weekendDays: number[] = [0, 6];
  dayStartHour: number = 6;
  dayEndHour: number = 22;

  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;



  CalendarView = CalendarView;

  viewDate: Date = new Date();

  today: number = Date.now();

  modalData: {
    action: string;
    event: CalendarEvent;
  };








  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    // {
    //   start: subDays(startOfDay(new Date()), 0),
    //   end: addDays(new Date(), 0),
    //   title: 'A 3 day event',
    //   color: colors.red,
    //   actions: this.actions,
    //   allDay: true,
    //   resizable: {
    //     beforeStart: true,
    //     afterEnd: true
    //   },
    //   draggable: true
    // },

    // {
    //   start: startOfDay(new Date()),
    //   title: 'An event with no end date',
    //   color: colors.yellow,
    //   actions: this.actions
    // },

    /*  {
       start: addDays(startOfMonth((new Date())), 5),
       end: subDays(endOfMonth((new Date())), 4),
       title: ' Schedule: 7:00 - 16:00', //'A long event that spans 2 months',
       color: colors.blue,
       allDay: true
     }, */
    /*     {
          title: 'No event end date',
          start: endOfHour(setMinutes(new Date(Date.parse("Octber 25 2019 02:00:00")), 1)),
          color: colors.red,
          actions: this.actions,
          resizable: {
            beforeStart: true,
            afterEnd: true
          },
          draggable: true
        }, */
    {
      start: startOfWeek(startOfHour(subHours(new Date(), 5)), { weekStartsOn: 1 }),
      end: endOfWeek(setHours(new Date(), 16), { weekStartsOn: 6 }),
      title: 'A Monday to Friday schedule',
      color: colors.yellow,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },

    {
      start: setHours(new Date("2019-10-01"), 7),
      end: setHours(new Date("2019-10-1"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.black,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-02"), 7),

      end: setHours(new Date("2019-10-2"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.black,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-03"), 7),

      end: setHours(new Date("2019-10-3"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.black,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-04"), 7),

      end: setHours(new Date("2019-10-04"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.black,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-09-30"), 7),

      end: setHours(new Date("2019-09-30"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm ',
      color: colors.black,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },


    //one month sched
    {
      start: setHours(new Date("2019-10-14"), 7),

      end: setHours(new Date("2019-10-14"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-15"), 7),

      end: setHours(new Date("2019-10-15"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-16"), 7),

      end: setHours(new Date("2019-10-16"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-17"), 7),

      end: setHours(new Date("2019-10-17"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-18"), 7),

      end: setHours(new Date("2019-10-18"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    /* {
      start: setHours(new Date("2019-10-19"), 7),

      end: setHours(new Date("2019-10-19"), 16),
      title: 'Rest Day',
      color: colors.blue,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }, */
    /*  {
       start: setHours(new Date("2019-10-20"), 7),
 
       end: setHours(new Date("2019-10-20"), 16),
       title: 'Rest Day',
       color: colors.blue,
       actions: this.actions,
       resizable: {
         beforeStart: true,
         afterEnd: true
       },
       draggable: true
     }, */
    {
      start: setHours(new Date("2019-10-21"), 7),

      end: setHours(new Date("2019-10-21"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-22"), 7),

      end: setHours(new Date("2019-10-22"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-23"), 7),

      end: setHours(new Date("2019-10-23"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-24"), 7),

      end: setHours(new Date("2019-10-24"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-25"), 7),

      end: setHours(new Date("2019-10-25"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    /*  {
       start: setHours(new Date("2019-10-26"), 7),
 
       end: setHours(new Date("2019-10-26"), 16),
       title: 'Rest Day',
       color: colors.blue,
       actions: this.actions,
       resizable: {
         beforeStart: true,
         afterEnd: true
       },
       draggable: true
     },
     {
       start: setHours(new Date("2019-10-27"), 7),
 
       end: setHours(new Date("2019-10-27"), 16),
       title: 'Rest Day',
       color: colors.blue,
       actions: this.actions,
       resizable: {
         beforeStart: true,
         afterEnd: true
       },
       draggable: true
     }, */
    {
      start: setHours(new Date("2019-10-28"), 7),

      end: setHours(new Date("2019-10-28"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-29"), 7),

      end: setHours(new Date("2019-10-29"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-30"), 7),

      end: setHours(new Date("2019-10-30"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-10-31"), 7),

      end: setHours(new Date("2019-10-31"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-1"), 7),

      end: setHours(new Date("2019-11-1"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    /* {
      start: setHours(new Date("2019-11-2"), 7),

      end: setHours(new Date("2019-11-2"), 16),
      title: 'Rest Day',
      color: colors.blue,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-3"), 7),

      end: setHours(new Date("2019-11-3"), 16),
      title: 'Rest Day',
      color: colors.blue,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }, */
    {
      start: setHours(new Date("2019-11-4"), 7),

      end: setHours(new Date("2019-11-4"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-5"), 7),

      end: setHours(new Date("2019-11-5"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-6"), 7),

      end: setHours(new Date("2019-11-6"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-7"), 7),

      end: setHours(new Date("2019-11-7"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-8"), 7),

      end: setHours(new Date("2019-11-8"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    /* {
      start: setHours(new Date("2019-11-9"), 7),

      end: setHours(new Date("2019-11-9"), 16),
      title: 'Rest Day',
      color: colors.blue,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-10"), 7),

      end: setHours(new Date("2019-11-10"), 16),
      title: 'Rest Day',
      color: colors.blue,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }, */
    {
      start: setHours(new Date("2019-11-11"), 7),

      end: setHours(new Date("2019-11-11"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-12"), 7),

      end: setHours(new Date("2019-11-12"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-13"), 7),

      end: setHours(new Date("2019-11-13"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: setHours(new Date("2019-11-14"), 7),

      end: setHours(new Date("2019-11-14"), 16),
      title: 'A Monday to Friday schedule with 7am to 4pm',
      color: colors.red,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },



  ];


  activeDayIsOpen: boolean = true;

  date1;
  date2;

  constructor(private modal: NgbModal) {
    //katabe ng new date which is 0 = hours and ung sa 45 is minutes
    const date1 = addDays(startOfDay(new Date()), 5);

    console.log(format(date1, 'DD.MM.YYYY hh:mm'));
    /*   const date1 = new Date();
      const date2 = addHours(new Date(), 5);
      console.log(`Date1 is ${isAfter(date1, date2) ? 'after' : 'before'} Date2`);
      console.log(`Date2 is ${isFuture(date2) ? 'not' : ''} in the past`); */
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }





  ngOnInit() {
  }

}


