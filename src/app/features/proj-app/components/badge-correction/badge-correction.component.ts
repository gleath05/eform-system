import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { setDate } from 'date-fns';

@Component({
  selector: 'app-badge-correction',
  templateUrl: './badge-correction.component.html',
  styleUrls: ['./badge-correction.component.scss']
})
export class BadgeCorrectionComponent implements OnInit {

  badgeCorrectionForm: FormGroup;
  isSubmitted: boolean;
  minDate = new Date(new Date().getTime() - (1 * 24 * 60 * 60 * 1000));


  // flags to group the fields
  Flag1 = true;
  Flag2 = true;
  Flag3 = true;

  constructor(private fb: FormBuilder) { }

  get formControls() {
    return this.badgeCorrectionForm.controls;
  }

  get reason() {
    return this.badgeCorrectionForm.get('reason');
  }

  function(x) {
    if (x == 1 || x == 2) {
      this.Flag1 = true;
      this.Flag2 = true;
      this.Flag3 = true;
      // reset form when user changes their reason to avoid invalid submits
      this.resetRadioReactiveValues();
      // set the validators back to normal
      this.badgeCorrectionForm.get('correctRecordBasedOnActualClockOut').setValidators([Validators.required]);
      this.badgeCorrectionForm.get('correctRecordBasedOnActualClockOut').updateValueAndValidity();
      this.badgeCorrectionForm.get('correctRecord_BOACO_time').setValidators([Validators.required]);
      this.badgeCorrectionForm.get('correctRecord_BOACO_time').updateValueAndValidity();
      // validators set to default part 2
      this.badgeCorrectionForm.get('correctRecordBasedOnActualClockIn').setValidators([Validators.required]);
      this.badgeCorrectionForm.get('correctRecordBasedOnActualClockIn').updateValueAndValidity();
      this.badgeCorrectionForm.get('correctRecord_BOACI_time').setValidators([Validators.required]);
      this.badgeCorrectionForm.get('correctRecord_BOACI_time').updateValueAndValidity();
    }
    else if (x == 3 || x == 5) {
      this.Flag1 = true;
      this.Flag2 = true;
      this.Flag3 = false;
      // reset form when user changes their reason
      this.resetRadioReactiveValues();
      // clear validators for flag 3 (actual clock out)     
      this.badgeCorrectionForm.get('correctRecordBasedOnActualClockOut').clearValidators();
      this.badgeCorrectionForm.get('correctRecordBasedOnActualClockOut').updateValueAndValidity();
      this.badgeCorrectionForm.get('correctRecord_BOACO_time').clearValidators();
      this.badgeCorrectionForm.get('correctRecord_BOACO_time').updateValueAndValidity();


    }
    else if (x == 4 || x == 6) {
      this.Flag1 = true;
      this.Flag2 = false;
      this.Flag3 = true;
      // reset form when user changes their reason
      this.resetRadioReactiveValues();
      // clear validators for flag 2 (actual clock in )
      this.badgeCorrectionForm.get('correctRecordBasedOnActualClockIn').clearValidators();
      this.badgeCorrectionForm.get('correctRecordBasedOnActualClockIn').updateValueAndValidity();
      this.badgeCorrectionForm.get('correctRecord_BOACI_time').clearValidators();
      this.badgeCorrectionForm.get('correctRecord_BOACI_time').updateValueAndValidity();
    }
  }

  resetRadioReactiveValues() {
    this.badgeCorrectionForm.reset();
  }
  onSubmitReset() {
    this.badgeCorrectionForm.reset();
    this.badgeCorrectionForm.patchValue({ reasonForBadgeCorrection: { reason: 'forgot to bring badge' } })
  }
  onSubmit(formValue) {
    this.isSubmitted = true;
    if (this.badgeCorrectionForm.valid) {
      console.log(formValue);
    } else {
      alert("please complete all the fields!");
    }
  }

  ngOnInit() {
    this.badgeCorrectionForm = this.fb.group({
      requestDate: [null, Validators.required],
      reasonForBadgeCorrection: this.fb.group({
        reason: 'forgot to bring badge',
        forgotToClockOutForBreak_GROUP: this.fb.group({
          firstBreak: [false],
          lunchBreak: [false],
          secondBreak: [false],
        }),
        forgotToClockInAfterBreak_GROUP: this.fb.group({
          firstBreak: [false],
          lunchBreak: [false],
          secondBreak: [false],
        }),

      }),
      dashboardRecordToBeCorrected: [null, Validators.required],
      dashboardRecordToBeCorrected_time: [null, Validators.required],
      correctRecordBasedOnActualClockIn: [null, Validators.required],
      correctRecord_BOACI_time: [null, Validators.required],
      correctRecordBasedOnActualClockOut: [null, Validators.required],
      correctRecord_BOACO_time: [null, Validators.required],
    });
  }
}

