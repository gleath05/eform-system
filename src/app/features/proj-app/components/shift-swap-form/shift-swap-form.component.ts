import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-shift-swap-form',
  templateUrl: './shift-swap-form.component.html',
  styleUrls: ['./shift-swap-form.component.scss']
})
export class ShiftSwapFormComponent implements OnInit {

  minDate = new Date();
  isSubmitted: boolean;
  SwapForm: FormGroup;

  shiftSwap = true;
  restdaySwap = false;
  schedSwap = false;


  constructor(private fb: FormBuilder) { }



  get formControls() {
    return this.SwapForm.controls;
  }

  get changeSwap() {
    return this.SwapForm.get('changeSwap');
  }

  swap(x) {
    if (x == 1) {
      this.shiftSwap = true;
      this.restdaySwap = false;
      this.schedSwap = false;
    }
    else if (x == 2) {
      this.shiftSwap = false;
      this.restdaySwap = true;
      this.schedSwap = false;
    }
    else if (x == 3) {
      this.shiftSwap = false;
      this.restdaySwap = false;
      this.schedSwap = true;
    }
  }
  onSubmitReset() {
    this.SwapForm.reset(this.swap);
    this.SwapForm.patchValue({ Swap:{changeSwap: 'shiftSchedule'}})
    
  }

  onSubmit(formValue) {
    this.isSubmitted = true;
    if (this.SwapForm.valid) 
    {
      console.log(formValue);
    } else {
      alert("please complete all the fields!");
    }
  }

  ngOnInit() {
    this.SwapForm = this.fb.group({
      Swap: this.fb.group({
      changeSwap: ['shiftSchedule']
      }),
      emp1Name: [null, Validators.required],
      emp1RequestedDate: [null, Validators.required],
      emp1currentShift: [null, Validators.required],
      emp1requestedShift: [null, Validators.required],
      restDayReason: [null, Validators.required],
      emp2Name: [null, Validators.required],
      emp2RequestedDate: [null, Validators.required],
      emp2currentShift: [null, Validators.required],
      emp2requestedShift: [null, Validators.required],
    })
  }
}
