import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-leave-forms',
  templateUrl: './leave-forms.component.html',
  styleUrls: ['./leave-forms.component.scss']
})
export class LeaveFormsComponent implements OnInit {

  leaveForm: FormGroup;
  isSubmitted: boolean;
  isCollapsed: boolean = true;
  ptoLeft = 7;
  minDate = new Date();

  radOtherLeave: boolean = false;

  constructor(private fb: FormBuilder) {

  }

  ngOnInit() {
    this.isCollapsed = false;

    this.leaveForm = this.fb.group({
      approval: [null, Validators.required],
      startDate: [null, Validators.required],
      endDate: [null, Validators.required],
      reason: [null, Validators.required],
      leaveChoices: [null, Validators.required],
      otherLeave: [null],
      returnDate: [null, Validators.required]
    });
    this.resetForm();
  }

  resetForm() {
    this.leaveForm.reset();
    this.leaveForm.setValue({
      approval: 'pre-approval',
      startDate: '',
      endDate: '',
      reason: '',
      leaveChoices: 'pto' ,
      otherLeave: '',
      returnDate: ''
    });
  }

  toggleCollapse(a) {
    console.log(a);
    if (a == 'pre') {
      this.isCollapsed = false;
      this.leaveForm.get('returnDate').setValidators([Validators.required]);
      this.leaveForm.get('returnDate').updateValueAndValidity();
      this.resetForm();
    }
    else {
      this.isCollapsed = true;
      this.leaveForm.get('returnDate').clearValidators();
      this.leaveForm.get('returnDate').updateValueAndValidity();
      this.resetForm();
    }
  }

  radioButtonClicked(rad) { 
    
    if (rad == 'otherLeave') {
      this.radOtherLeave = true;
      this.leaveForm.get('otherLeave').setValidators([Validators.required]);
      this.leaveForm.get('otherLeave').updateValueAndValidity();
    }
    else { 
      this.radOtherLeave = false;
      this.leaveForm.get('otherLeave').clearValidators();
      this.leaveForm.get('otherLeave').updateValueAndValidity();
    }
  }




  get formControls() {
    return this.leaveForm.controls;
  }

  onSubmit(formValue) {
    this.isSubmitted = true;
    if (this.leaveForm.valid) {
      alert("Leave Request Submitted!");
      console.log(formValue);
      this.isCollapsed = false;
      this.radOtherLeave = false;
    } else {
      console.error(formValue);

    }
  }

}
