// components
import { AppComponent } from './app.component';

//modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SideNavComponent } from './features/proj-app/components/side-nav/side-nav.component';
import { ProjAppModule } from './features/proj-app/proj-app.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule, MatInputModule, MatCardModule, MatButtonModule, MatSidenavModule, MatToolbarModule, MatIconModule } from '@angular/material';
import { LoginComponent } from './features/proj-app/components/login/login.component';
import { ToastrModule } from 'ngx-toastr';
import { MainMenuComponent } from './features/proj-app/container/main-menu/main-menu.component';

@NgModule({
  declarations: [
    AppComponent,
   LoginComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ProjAppModule,
    ReactiveFormsModule,
    FormsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    ToastrModule.forRoot()
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
